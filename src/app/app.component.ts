import { Component } from '@angular/core';
import {SocketService} from './services/socket.service'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';

	subscriptions:Array<any> = [];

	constructor(private socket:SocketService){

		this.subscriptions.push(this.socket.on('user.system.create').subscribe(val=>{

			console.log(val);

		}));

	}

	ngOnDestroy() {

		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}

	}

}
