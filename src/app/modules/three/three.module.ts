import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThreeComponent } from './components/three.component';
import { ThreeService } from './services/three.service';

@NgModule({
	imports: [
		CommonModule,
	],
	exports: [ThreeComponent],
	declarations: [ThreeComponent],
	providers:[ThreeService]
})
export class ThreeModule { }
