import { Injectable } from '@angular/core';
import * as THREE from 'three';
import * as TWEEN from '@tweenjs/tween.js';
import OrbitControls from 'orbit-controls-es6';

@Injectable()
export class ThreeService {

	// scene, camera
	public scene:THREE.Scene = new THREE.Scene();
	public camera:THREE.PerspectiveCamera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10200);
	
	public controls:OrbitControls;
	public renderer = new THREE.WebGLRenderer({
		antialias: true
	});

	// mouse move detect
	public mouse = {
		click:new THREE.Vector2(),
		move:new THREE.Vector2(),
	};

	// intersects
	public intersects = {
		click:[],
		over:[],
	};

	// FPS
	public FPS:number = 0;
	private fpsCount:number = 1;
	private lastLoop:number = Date.now();

	// shaders & textures
	private shaders:Array<{name:string,vertex?:string,fragment?:string}> = [];
	private textures:Array<{name:string,texture:any}> = [];

	// scene categories
	private categories = [];

	// model origin
	private MODELORIGIN ='http://uu.api/models/';

	// texture, json and shader cache var
	private cache:{textures:{},json:{},shaders:{}} = {
		textures:{},
		json:{},
		shaders:{}
	};

	constructor() {

		this.controls = new OrbitControls(this.camera, this.renderer.domElement);
		this.camera.position.set(100,0,0);

		this.controls.enabled = true;
		delete(this.controls.mouseButtons.PAN);

	}

	/** RAYCASTING **/

	private raycastObjParent(obj){

		if(obj.parent.type === 'Scene') {
			return null;
		}

		let cIdx = this.categories.findIndex(x=>x.name === obj.parent.name);
		if(cIdx > -1) {
		
			return null;
		
		} else {
			if(obj.parent.type !== 'Group') {
				return obj.parent.name;		
			} else {

				return this.raycastObjParent(obj.parent);
			
			}
		
		}

	}

	private raycastObj(obj,category) {

		let that = this;
		if(obj.children.length > 0){

			let rcC = new THREE.Raycaster();
			rcC.setFromCamera( this.mouse.click, this.camera );
			let clickIntersects = rcC.intersectObjects(obj.children);
			clickIntersects.forEach(function(is:any){
				is.category = category;
				is.parent = that.raycastObjParent(is.object);
				that.intersects.click.push(is);
			});

			let rcM = new THREE.Raycaster();			
			rcM.setFromCamera( this.mouse.move, this.camera );
			let moveInstersects = rcM.intersectObjects(obj.children);
			moveInstersects.forEach(function(is:any){
				is.category = category;
				is.parent = that.raycastObjParent(is.object);
				that.intersects.over.push(is);
			});

			obj.children.forEach(function(child){
				if(child.children.length>0){
					that.raycastObj(child,category);
				}
			});	
		
		}

	}

	private raycast(){

		this.intersects.click = [],
			this.intersects.over = [];

		this.categories.forEach(cat=>{

			if(cat.raycastable){

				let isCat = this.scene.getObjectByName(cat.name);
				if(isCat){

					this.raycastObj(isCat,cat.name);

				}

			}

		});
		this.intersects.click.sort((a:any,b:any)=>{
  			return a.distance - b.distance;
		});

	}

	/* Auto Updater */

	private autoUpdate(){

		this.categories.forEach(category=>{

			let sceneGroup = this.scene.getObjectByName(category.name);
			if(sceneGroup) this.autoUpdateWalk(sceneGroup);

		});
	
	}

	private autoUpdateWalk(obj) {
		
		if(obj['update']){
			obj['update'](obj);
		}
		if(obj['uniforms']){

			if(obj['uniforms']['master']) {
				
				if(obj['uniforms']['master'].time){
					
					let updT = 0.01;
					if(obj['uniforms']['options']){
						if(obj['uniforms']['options'].time){
							updT = obj['uniforms']['options'].time;
						}
					}
					obj['uniforms']['master'].time.value += updT;
				}

			}

		}
		if(obj.children.length>0){

			obj.children.forEach(child=>{
				this.autoUpdateWalk(child);
			});
		
		}
		return;
	
	}

	/** EXPOSED FUNCTIONS **/

	/* Loaders */

	// load shader
	public loadShader(f,cb){

		let vLoader = new THREE.FileLoader(THREE.DefaultLoadingManager);
		vLoader.setResponseType('text');
		vLoader.load(f,fActual=>{
			cb(fActual,null);
		},null,e=>{
			cb(null,e);
		});

	}

	// load texture
	public loadTexture(url:string,cb:any) {

		let loader = new THREE.TextureLoader();
		loader.crossOrigin = '';
		loader.setPath(this.MODELORIGIN);

		let texture = loader.load(url,function(texture) {
			cb(texture,null);
		},null,e=>{
			cb(null,e);
		});

	}

	public getTextures(url:Array<{name:string,url:string}>,overwrite?:boolean){

		let that = this;
		return new Promise((resolve,reject)=>{

			let p = [];
			let r = {};
			url.forEach(u=>{

				p.push(new Promise((res,rej)=>{

					that.loadTexture(u.url,(texture,err)=>{
						if(!err) {
							texture.name = u.name;
							r[u.name] = texture;
							res();
						} else {
							rej(err);
						}

					});

				}));
			});
			Promise.all(p)
				.then(()=>{
					resolve(r);
				})
				.catch(e=>{
					reject(e);
				})

		});

	}

	public addShaders(vfObj:Array<Array<{name:string,type:string,url:string}>>){

		let that = this;
		return new Promise((resolve,reject)=>{

			let p = [];
			vfObj.forEach(u=>{
			
				for(let i=0,c=u.length;i<c;i++){

					let vfObjItem = u[i];

					p.push(new Promise((res,rej)=>{

						that.loadShader(vfObjItem.url,function(result,e){

							if(!e){

								let shPush = {name:vfObjItem.name};
								shPush[vfObjItem.type] = result;

								let sIdx = that.shaders.findIndex(x=>x.name === vfObjItem.name);
								if(sIdx < 0) {

									that.shaders.push(shPush);
								
								} else {

									if(!that.shaders[sIdx][vfObjItem.type]) that.shaders[sIdx][vfObjItem.type] = result;
									if(result !== that.shaders[sIdx][vfObjItem.type]) {

										that.shaders[sIdx][vfObjItem.type] = result;
									
									}

								}
								res();

							} else {

								rej(e);
							
							}

						});

					}));

				}

			});

			Promise.all(p)
				.then(()=>{
					resolve();
				})
				.catch(e=>{
					reject(e);
				})

		});

	}

	public getShader(name){

		return this.shaders.find(x=>x.name === name);

	}

	public addCategory(category:string,options:{raycastable?:boolean,autoUpdate?:boolean} = {}){

		return new Promise((resolve,reject)=>{

			try {

				let nObj = new THREE.Group();
				nObj.name = category;

				let categoryObj:any = {
					name:category,
				};
				if(options.raycastable){
					categoryObj.raycastable = true;
				}
				if(options.raycastable){
					categoryObj.autoUpdate = true;
				}

				let catCheck = this.categories.findIndex(x=>x.name === category);
				if(catCheck < 0){
					this.categories.push(categoryObj);	
				}
				this.scene.add(nObj);

				resolve();

			} catch(e) {

				reject(e);

			}

		});

	}

	public addToCategory(category:string,obj:any,options:{raycastable?:boolean,autoUpdate?:boolean} = {}) {

		let foundCategory = this.scene.getObjectByName(category);
		if(!foundCategory) {

			this.addCategory(category,options)
				.then(()=>{
					this.addToCategory(category,obj,options);		
				});

		} else {

			if(foundCategory.type !== 'Group'){

				let foundGroup = foundCategory.getObjectByName(category + '-group');
				if(!foundGroup) {
					
					let nObj = new THREE.Group();
					nObj.name = category + '-group';
					nObj.add(obj);
					foundCategory.add(nObj);	

				} else {

					foundGroup.add(obj);

				}

			} else {

				foundCategory.add(obj);	

			}

		}

	}

	public cleanScene(excludes?:any){

		if(!excludes) excludes = [];
		if(excludes.prop && excludes.prop.constructor !== Array) excludes = [excludes];
		let cont = false,
			removes = [];
		if(this.scene){
			if(this.scene.children.length>0) cont = true;
		}
		if(cont){
			for(let i=0;i<this.scene.children.length;i++){
				if(excludes.indexOf(this.scene.children[i].name) < 0){
					this.scene.remove(this.scene.children[i]);
				}
			}			
		}
		return;

	}

	private updateFPS(){

		if(Date.now() - this.lastLoop >= 1000) {
			
			this.FPS = this.fpsCount;
			this.fpsCount = 0;
			this.lastLoop = Date.now();

		} else {

			this.fpsCount++;

		}

	}

	public update(){

		this.raycast();

		this.autoUpdate();

		TWEEN.update();

		let skybox = this.scene.getObjectByName('skybox');
		if(skybox) {
			skybox.position.set(this.camera.position.x,this.camera.position.y,this.camera.position.z);
		}

		this.updateFPS();

	}

}
