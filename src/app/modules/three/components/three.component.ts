import { Component, OnInit, Input, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';
import { ThreeService } from '../services/three.service';

import * as THREE from 'three';

@Component({
	selector: 'three-box',
	templateUrl: './three.component.html',
	styleUrls: ['./three.component.css']
})
export class ThreeComponent {

	// @Input() toggleVR: boolean = false;  
	// @Input() toggleControls: boolean = true;
	@Output() threeNotification: EventEmitter<any> = new EventEmitter();
	
	private isReady:boolean = false;
	private id:number;

	constructor(private svc:ThreeService, private element:ElementRef) {
		
		this.element.nativeElement.appendChild(this.svc.renderer.domElement);

		this.resetWidthHeight();
		
		this.render();

	}

	ngAfterContentInit() {
		this.threeNotification.emit({ready:true});
	}

	ngOnChanges(changes) {}

	ngOnDestroy(){
		cancelAnimationFrame( this.id );
	}

	render() {

		this.svc.update();

		this.svc.renderer.render(this.svc.scene, this.svc.camera);

		this.svc.controls.update();

		this.id = requestAnimationFrame(() => this.render());

	}

	@HostListener('window:resize')
	@HostListener('window:vrdisplaypresentchange')
	resetWidthHeight() {
		
		this.svc.renderer.setSize(window.innerWidth, window.innerHeight);
		this.svc.renderer.setPixelRatio(Math.floor(window.devicePixelRatio));

		this.svc.camera.aspect = window.innerWidth / window.innerHeight;
		this.svc.camera.updateProjectionMatrix();
		
	}

	@HostListener('click', ['$event'])
	onMouseClick(e) {
		this.svc.mouse.click.x = ( e.clientX / window.innerWidth ) * 2 - 1;
		this.svc.mouse.click.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
	}

	@HostListener('mousemove', ['$event'])
	onMouseMove(e) {
		this.svc.mouse.move.x = ( e.clientX / window.innerWidth ) * 2 - 1;
		this.svc.mouse.move.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
	}	

}


// var fps = {	
// 	startTime : 0,	
// 	frameNumber : 0,	
// 	getFPS : function(){		
// 		this.frameNumber++;		
// 		var d = new Date().getTime(),			
// 		currentTime = ( d - this.startTime ) / 1000,			
// 		result = Math.floor( ( this.frameNumber / currentTime ) );

// 		if( currentTime > 1 ){
// 			this.startTime = new Date().getTime();
// 				this.frameNumber = 0;		
// 		}		
// 		return result;	
// 	}	
// };

