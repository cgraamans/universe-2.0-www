import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewportComponent } from './pages/viewport/viewport.component'
import { HomeComponent } from './pages/home/home.component'

const routes: Routes = [

	{ path: '', component: HomeComponent },
	{ path: 'galaxy/:cluster/:star/:obj', component: ViewportComponent },
	{ path: 'galaxy/:cluster/:star', component: ViewportComponent },
	{ path: 'galaxy/:cluster', component: ViewportComponent },
	{ path: 'galaxy', component: ViewportComponent },

];

@NgModule({
	exports: [
		RouterModule
	],
	imports: [ 
		RouterModule.forRoot(routes) 
	],

})

export class AppRoutingModule { }
