import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nav-focus',
  templateUrl: './nav-focus.component.html',
  styleUrls: ['./nav-focus.component.css']
})
export class NavFocusComponent implements OnInit {

	@Input() cluster:App.Obj.NameStub|boolean = false;
	@Input() star:App.Obj.NameStub|boolean = false;;
	@Input() obj:App.Obj.NameStub|boolean = false;

	constructor() {}

	ngOnInit() {}

}