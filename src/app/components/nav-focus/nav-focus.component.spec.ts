import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavFocusComponent } from './nav-focus.component';

describe('NavFocusComponent', () => {
  let component: NavFocusComponent;
  let fixture: ComponentFixture<NavFocusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavFocusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavFocusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
