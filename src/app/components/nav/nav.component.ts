import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
	selector: 'nav',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		this.slideMenuToggle = false;
	}

	slideMenu() {
		this.slideMenuToggle = !this.slideMenuToggle;
	}

	@HostBinding('class.top-nav-minify') slideMenuToggle:boolean = false;

}
