import { Component, OnInit, Input, Output, HostBinding, EventEmitter } from '@angular/core';

@Component({
  selector: 'viewport-debug',
  templateUrl: './viewport-debug.component.html',
  styleUrls: ['./viewport-debug.component.css']
})
export class ViewportDebugComponent implements OnInit {

	@Input() FPS:number = 0;
	@Input() isClosed:boolean = false;

	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();

	@HostBinding('class.minify') isMinified:boolean = false;
	@HostBinding('class.close')
		get cC(){
			return !this.isClosed;
		};

	constructor() {}
	ngOnInit() {}
	
	minify() {

		this.isMinified = !this.isMinified;

	}

	close() {

		this.interfaceToggles.emit({toggle:'debug'});

	}

}
