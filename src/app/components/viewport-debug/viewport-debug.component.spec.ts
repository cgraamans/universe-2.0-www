import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewportDebugComponent } from './viewport-debug.component';

describe('ViewportDebugComponent', () => {
  let component: ViewportDebugComponent;
  let fixture: ComponentFixture<ViewportDebugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewportDebugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewportDebugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
