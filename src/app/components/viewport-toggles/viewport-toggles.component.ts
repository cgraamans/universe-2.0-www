import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'viewport-toggles',
  templateUrl: './viewport-toggles.component.html',
  styleUrls: ['./viewport-toggles.component.css']
})
export class ViewportTogglesComponent implements OnInit {
	
	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();
	@Input() interfaceDisplay:{} = {};

	constructor() {}

	ngOnInit() {}

	toggle(toggleElement){
		this.interfaceToggles.emit({toggle:toggleElement});
	}

}
