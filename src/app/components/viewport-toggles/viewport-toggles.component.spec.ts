import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewportTogglesComponent } from './viewport-toggles.component';

describe('ViewportTogglesComponent', () => {
  let component: ViewportTogglesComponent;
  let fixture: ComponentFixture<ViewportTogglesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewportTogglesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewportTogglesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
