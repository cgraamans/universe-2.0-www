import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavAudioComponent } from './nav-audio.component';

describe('NavAudioComponent', () => {
  let component: NavAudioComponent;
  let fixture: ComponentFixture<NavAudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavAudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
