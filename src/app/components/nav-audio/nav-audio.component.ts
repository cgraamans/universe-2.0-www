import { Component, OnInit, Input, Output, HostBinding, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@Component({
  selector: 'nav-audio',
  templateUrl: './nav-audio.component.html',
  styleUrls: ['./nav-audio.component.css']
})
export class NavAudioComponent implements OnInit {

	@Input() isClosed:boolean = false;

	@Input() audio:[App.Obj.NameUrl];
	current:BehaviorSubject<App.Obj.NameUrl>;

	@ViewChild('player') player: ElementRef;

	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();

	@HostBinding('class.minify') isMinified:boolean = false;
	@HostBinding('class.close')
		get cC(){
			return !this.isClosed;
		};

	constructor() {}

	ngOnInit() {

		if(this.audio){

			this.current = new BehaviorSubject(this.audio[0]);
		
		}

	}


	minify() {

		this.isMinified = !this.isMinified;

	}

	close() {

		this.interfaceToggles.emit({toggle:'navAudio'});

	}

	setAudio(url){

		this.current.next(url);
	
	}



	getAudio(){
	
		return this.current.getValue();
	
	}

	hasCurrentAudio(){
	
		return this.current.getValue() ? true : false;
	
	}

	getCurrentAudioTrackUrl(){

		let current = this.current.getValue();
		return current ? current.url : false;

	}

	getCurrentAudioTrackName() {

		let current = this.current.getValue();
		return current ? current.name : false;

	}

	ngOnDestroy(){
	
		this.current.unsubscribe();
	
	}

}
