import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavStarsComponent } from './nav-stars.component';

describe('NavStarsComponent', () => {
  let component: NavStarsComponent;
  let fixture: ComponentFixture<NavStarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavStarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavStarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
