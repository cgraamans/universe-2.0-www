import { Component, OnInit, Input, Output, HostBinding, EventEmitter } from '@angular/core';

@Component({
  selector: 'nav-stars',
  templateUrl: './nav-stars.component.html',
  styleUrls: ['./nav-stars.component.css']
})
export class NavStarsComponent implements OnInit {

	@Input() stars:Array<App.Obj.NameStub>;
	@Input() cluster:{stub:string};

	@Input() isClosed:boolean = false;

	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();

	@HostBinding('class.minify') isMinified:boolean = false;
	@HostBinding('class.close')
		get cC(){
			return !this.isClosed;
		};

	constructor() {}
	ngOnInit() {}

	minify() {

		this.isMinified = !this.isMinified;

	}

	close() {

		this.interfaceToggles.emit({toggle:'navStars'});

	}

}
