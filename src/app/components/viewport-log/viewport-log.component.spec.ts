import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewportLogComponent } from './viewport-log.component';

describe('ViewportLogComponent', () => {
  let component: ViewportLogComponent;
  let fixture: ComponentFixture<ViewportLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewportLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewportLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
