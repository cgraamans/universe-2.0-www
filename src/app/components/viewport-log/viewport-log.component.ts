import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'viewport-log',
  templateUrl: './viewport-log.component.html',
  styleUrls: ['./viewport-log.component.css'],
})
export class ViewportLogComponent implements OnInit {

	@Input() logs:Array<ViewPortLogItem> = [];
	@Input() isClosed:boolean = false;

	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();

	@HostBinding('class.minify') isMinified:boolean = false;
	@HostBinding('class.close')
		get cC(){
			return !this.isClosed;
		};

	constructor() {}
	ngOnInit() {}
	
	minify() {

		this.isMinified = !this.isMinified;

	}

	close() {

		this.interfaceToggles.emit({toggle:'logs'});

	}

	dtToDate(dateTime){

		let dt = new Date(dateTime);
		return ('0' + dt.getHours()).slice(-2) + ':' + ('0' + dt.getMinutes()).slice(-2) + ':' + ('0' + dt.getSeconds()).slice(-2) + ':' + ('0' + dt.getMilliseconds());

	}

}

export interface ViewPortLogItem {

	dt:number;
	line:string;
	component?:string;

}