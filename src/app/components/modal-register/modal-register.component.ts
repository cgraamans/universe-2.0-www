import { Component, OnInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef, ApplicationRef, NgZone } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {FormControl} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';


@Component({
	selector: 'app-modal-register',
	templateUrl: './modal-register.component.html',
	styleUrls: ['./modal-register.component.css']
})
export class ModalRegisterComponent implements OnInit {

	private formObj:App.FormObj = {name:null,password:null,persistent:true};
	private valid:App.FormObjValid = {name:false,password:false};

	private subscriptions:any = [];

  	@ViewChild('inputRegName') inputRegNameRef: ElementRef;
  	@ViewChild('inputRegPwd') inputRegPwdRef: ElementRef;

	constructor(private socket:SocketService,public bsModalRef:BsModalRef,private ngzone: NgZone, private cdref: ChangeDetectorRef,
    private appref: ApplicationRef) {}

	ngAfterViewInit() {

		this.ngzone.runOutsideAngular( () => {

			this.subscriptions.push(
				Observable.fromEvent(this.inputRegNameRef.nativeElement, 'keyup')
					.debounceTime(300)
					.subscribe(()=>{
						this.socket.emit('auth.verify.name',{name:this.formObj.name});
					})
			);
			this.subscriptions.push(
				Observable.fromEvent(this.inputRegPwdRef.nativeElement, 'keyup')
					.debounceTime(300)
					.subscribe(()=>{
						this.socket.emit('auth.verify.password',{password:this.formObj.password});
					})
			);

		});

	}

	ngOnInit() {

		this.subscriptions.push(this.socket.on('auth.verify.name').subscribe(val=>{
			val.ok ? this.valid.name = true : this.valid.name = false;
		}));
		this.subscriptions.push(this.socket.on('auth.verify.password').subscribe(val=>{
			val.ok ? this.valid.password = true : this.valid.password = false;
		}));
		this.subscriptions.push(this.socket.on('auth.register').subscribe(val=>{
			console.log('auth.register',val);
		}));

	}

	submit(){
		this.socket.emit('auth.register',this.formObj);
	}

	isEmpty(){
		return !this.formObj.password && !this.formObj.name && this.valid.name && this.valid.password;
	}

	ngOnDestroy() {

		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}

	}


}
