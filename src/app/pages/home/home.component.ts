import { Component, OnInit } from '@angular/core';

import { DataService } from '../../services/data.service';
import { SocketService } from '../../services/socket.service';

import { ModalRegisterComponent } from '../../components/modal-register/modal-register.component';
import { ModalLoginComponent } from '../../components/modal-login/modal-login.component';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	private subscriptions = [];
	private intervals = [];

	public bsModalRef: BsModalRef;
	public isLoggedIn:boolean = false;
	public activeClusters = [];
	public refreshTime = 3000;

	constructor(private data:DataService,private modalService:BsModalService, private socket:SocketService) { 
		data.getAuthObs().subscribe(obs=>{
			if(obs) {
				this.isLoggedIn = true;
			} else {
				this.isLoggedIn = false;
			}
		});
	}

	ngOnInit() {
		
		this.subscriptions.push(this.socket.on('get.clusters.active').subscribe(val=>{
			if(val.ok && val.clusters){
				this.activeClusters = val.clusters;
			}
		}));
		this.socket.emit('get.clusters.active',{});	
		this.intervals.push(setInterval(()=>{
			this.socket.emit('get.clusters.active',{});
		},this.refreshTime));

	}

	ngOnDestroy(){
		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}
		for(let i=0,c=this.intervals.length;i<c;i++){
			clearInterval(this.intervals[i]);
		}		
	}

	openLogin(){
	    this.bsModalRef = this.modalService.show(ModalLoginComponent);
	}

	openRegister(){
	    this.bsModalRef = this.modalService.show(ModalRegisterComponent);
	}

}
