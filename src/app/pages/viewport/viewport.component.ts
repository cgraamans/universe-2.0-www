import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SocketService } from '../../services/socket.service';
import { ThreeParserService } from '../../services/three-parser.service';

import { ThreeModule } from '../../modules/three/three.module';

import { NavComponent } from '../../components/nav/nav.component';
import { NavStarsComponent } from '../../components/nav-stars/nav-stars.component';
import { NavFocusComponent } from '../../components/nav-focus/nav-focus.component';
import { NavAudioComponent } from '../../components/nav-audio/nav-audio.component';

import { ViewportLogComponent } from '../../components/viewport-log/viewport-log.component';
import { ViewportTogglesComponent } from '../../components/viewport-toggles/viewport-toggles.component';

@Component({
	selector: 'app-viewport',
	templateUrl: './viewport.component.html',
	styleUrls: ['./viewport.component.css']
})
export class ViewportComponent implements OnInit {

	subscriptions = [];
	routeParams:App.ViewPort.RouteParams = {};

	TOFile:App.UU.TS.FileStore = {

		shaders:{
			stars:[
				{
					name:'star-surface',
					type:'fragment',
					url:'http://uu.api/models/shaders/star-surface.frag',
				},
				{
					name:'star-surface',
					type:'vertex',
					url:'http://uu.api/models/shaders/star-surface.vert',
				},
			],
			skybox:[
				{
					name:'skybox',
					type:'fragment',
					url:'http://uu.api/models/shaders/skybox.frag',
				},
				{
					name:'skybox',
					type:'vertex',
					url:'http://uu.api/models/shaders/skybox.vert',
				},
			],
		},
		textures:{
			low:{
				stars:[

					{
						name:'star-shine',
						url:'/objects/low/stars/shine.png',
					},
					{
						name:'star-surface',
						url:'/objects/low/stars/sun_surface.jpg',
					},

				],
				tracks:[

					{
						name:'track-point',
						url:'/objects/low/point.png'
					}

				],
			}
		}

	};

	audio:Array<App.Obj.NameUrl> = [
		{name:'Shadowlands Codex',url:'http://uu.api/music/Shadowlands%207%20-%20Codex.mp3'}
	];

	logs = [];
	interfaceDisplay = {
		logs:false,
		navAudio:false,
		navStars:false,
		debug:true,
	};

	RES:string = "low";

	constructor(private socket:SocketService,private ThreeParser:ThreeParserService,private route:ActivatedRoute){

		window['DEBUG'] = true;

	}

	ngOnInit() {

		// route param changes
		this.subscriptions.push(this.route.params.subscribe(routeParams => {

			this.ThreeParser.resetScene();

			this.routeParams = routeParams;

			let clusterParams:{name?:string} = {};
			if(routeParams.cluster) {
				clusterParams.name = routeParams.cluster
			};
			this.socket.emit('get.cluster',clusterParams);
			this.logs.push({dt:(new Date()).getTime(),line:'requested cluster'});

			if(routeParams.cluster) {
			
				let starParams:{cluster:string,star?:string} = {
					cluster:routeParams.cluster,
				};
				if(routeParams.star) {
					starParams.star = routeParams.star;
				}
				console.log('EMIT STAR',starParams);
				this.socket.emit('get.stars',starParams);
				this.logs.push({dt:(new Date()).getTime(),line:'requested stars'});

			}

			if(routeParams.star) {

				let objParams:{star:string,obj?:string} = {
					star:routeParams.star,
				};

			}

		}));

		this.subscriptions.push(this.socket.on('get.stars').subscribe(val=>{

			if(val.ok === true) {
				
				this.logs.push({dt:(new Date()).getTime(),line:'stars found'});

				try {

					// add assets for clusters
					let assetList = {
						shaders:[],
						images:[]
					};
					assetList.shaders.push(this.TOFile.shaders.stars);
					assetList.images = assetList.images.concat(this.TOFile.textures[this.RES].stars);

					this.ThreeParser.setAssets(assetList)
						.then(()=>{

							this.logs.push({dt:(new Date()).getTime(),line:'star assets added'});
							
							console.log('stars',val.stars);

							let that = this;
							this.ThreeParser.setStars(val.stars)
								.then(function(){

									that.logs.push({dt:(new Date()).getTime(),line:'stars added'});

									// emit getPlanet
									let objParams:{focus:string} = {focus:that.ThreeParser.stars[0].stub};								
									that.socket.emit('get.obj',objParams);

									that.logs.push({dt:(new Date()).getTime(),line:'requested objects'});


								})
								.catch(e=>{

									this.logs.push({dt:(new Date()).getTime(),line:e});

								});

						})
						.catch(e=>{

							this.logs.push({dt:(new Date()).getTime(),line:e});

						});

				} catch(e) {

					this.logs.push({dt:(new Date()).getTime(),line:e});

				}

			} else {

				this.logs.push({dt:(new Date()).getTime(),line:val.error});
			
			}

		}));

		this.subscriptions.push(this.socket.on('get.obj').subscribe(val=>{

			if(val.ok === true) {

				this.logs.push({dt:(new Date()).getTime(),line:val.o.length+' objects found for star'});

				try {

					// add assets for clusters
					let assetList = {
						shaders:[],
						images:[]
					};
					assetList.shaders.push(this.TOFile.shaders.stars);
					assetList.images = assetList.images.concat(this.TOFile.textures[this.RES].stars);

					this.ThreeParser.setAssets(assetList)
						.then(()=>{

							this.logs.push({dt:(new Date()).getTime(),line:'star assets added'});

							let that = this;
							this.ThreeParser.setObjects(val.o,val.f)
								.then()
								.catch(e=>{

								});

						})
						.catch(e=>{

						});

				} catch(e) {

				}

			} else {

				console.log('Objects - ERR',val);
				this.logs.push({dt:(new Date()).getTime(),line:val.error});
			
			}

		}));

		this.subscriptions.push(this.socket.on('get.cluster').subscribe(val=>{

			if(val.ok === true){

				if(this.ThreeParser.cluster) {
					if(this.ThreeParser.cluster.stub){
						if (val.cluster.stub === this.ThreeParser.cluster.stub) {
							return
						} else {
							// remove cluster
						}
					}
				}

				this.logs.push({dt:(new Date()).getTime(),line:val.cluster.name+' found'});

				try {

					// add assets for clusters
					let assetList = {
						shaders:[],
						images:[
							{
								name:'skybox',
								url:'skybox/skybox'+val.cluster.skybox+'.jpg'
							}
						]
					};
					assetList.shaders.push(this.TOFile.shaders.skybox);

					this.ThreeParser.setAssets(assetList)
						.then(()=>{

							this.logs.push({dt:(new Date()).getTime(),line:'skybox assets loaded'});
							this.ThreeParser.setSkyBox()
								.then(()=>{

									this.ThreeParser.cluster = val.cluster;
									this.logs.push({dt:(new Date()).getTime(),line:'skybox loaded'});
								
								})
								.catch(e=>{

									this.logs.push({dt:(new Date()).getTime(),line:e});

								});
						})
						.catch(e=>{

							this.logs.push({dt:(new Date()).getTime(),line:e});

						});

					} catch(e) {

						this.logs.push({dt:(new Date()).getTime(),line:e});

					}

					// assetList.shaders.push(this.files.shaders.stars);
					// this.ThreeParser.updateStars(val.stars);	

			} else {

				this.logs.push({dt:(new Date()).getTime(),line:val.error});
			
			}

		}));

	}

	ngOnDestroy() {

		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}

	}

	//
	// Component Output toggles
	//

	interfaceToggles(evt){

		if(evt.toggle){
			this.interfaceDisplay[evt.toggle]? this.interfaceDisplay[evt.toggle] = false : this.interfaceDisplay[evt.toggle] = true;
		}

	}

	threeNotification(evt){

		if(evt.ready){
			this.logs.push({dt:(new Date()).getTime(),line:'renderer ready'});
		}

	}

	getStars(){
		return this.ThreeParser.stars;
	}

	getTopStar(){
		if(this.ThreeParser.stars.length>0){
			return this.ThreeParser.stars[0];
		}
	}

	getCluster(){
		return this.ThreeParser.cluster;
	}

	isDebug(){
		return window['DEBUG'] ? true : false;
	}

	getFPS() {
		return this.ThreeParser.getFPS();
	}

}