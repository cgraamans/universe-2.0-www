import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LocalStorageModule } from 'angular-2-local-storage';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AngularDraggableModule } from 'angular2-draggable';

import { ThreeModule } from './modules/three/three.module';

import { SocketService } from './services/socket.service';
import { DataService } from './services/data.service';
import { ThreeParserService } from './services/three-parser.service';

import { AppRoutingModule } from './app-routing.module';

import { ViewportComponent } from './pages/viewport/viewport.component';
import { HomeComponent } from './pages/home/home.component';

import { NavComponent } from './components/nav/nav.component';
import { ModalRegisterComponent } from './components/modal-register/modal-register.component';
import { ModalLoginComponent } from './components/modal-login/modal-login.component';
import { ViewportLogComponent } from './components/viewport-log/viewport-log.component';
import { ViewportTogglesComponent } from './components/viewport-toggles/viewport-toggles.component';
import { NavStarsComponent } from './components/nav-stars/nav-stars.component';
import { NavFocusComponent } from './components/nav-focus/nav-focus.component';
import { NavAudioComponent } from './components/nav-audio/nav-audio.component';
import { ViewportDebugComponent } from './components/viewport-debug/viewport-debug.component';

const config: SocketIoConfig = { url: 'http://192.168.178.248:9001', options: {} };

@NgModule({
	declarations: [
		AppComponent,
		ViewportComponent,
		NavComponent,
		HomeComponent,
		ModalRegisterComponent,
		ModalLoginComponent,
		ViewportLogComponent,
		ViewportTogglesComponent,
		NavStarsComponent,
		NavFocusComponent,
		NavAudioComponent,
		ViewportDebugComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		SocketIoModule.forRoot(config),
        LocalStorageModule.withConfig({
            prefix: 'app',
            storageType: 'localStorage'
        }),
        ThreeModule,
        ButtonsModule,
        ModalModule.forRoot(),
        FormsModule,
        AngularDraggableModule,
        BsDropdownModule.forRoot()
	],
	providers: [SocketService, DataService, ThreeParserService],
	bootstrap: [AppComponent],
	entryComponents:[ModalRegisterComponent,ModalLoginComponent]
})
export class AppModule { }
