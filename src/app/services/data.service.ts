import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class DataService {

	private auth:BehaviorSubject<Object> = new BehaviorSubject(null);
	constructor(private localStorageService: LocalStorageService) { }

// TODO:
// - audio
// - mapview

	setAuth(data?){

		if(!data){

			this.auth.next(null);

		} else {

			if(!data.auth || !data.apiId || !data.token || !data.name) this.auth.next(null);

			this.auth.next({
				auth:data.auth,
				apiId:data.apiId,
				token:data.token,
				name:data.name	
			});
			console.log('LOGGED IN');

		}

	}

	getAuth() {

		return this.auth.getValue();

	}

	getAuthObs() {

		return this.auth.asObservable();

	}

	getStore(keyName:string){

		return this.localStorageService.get(keyName);

	}

	setStore(keyName:string,data:{}){

		return this.localStorageService.set(keyName,data);

	}
}
