import { Injectable } from '@angular/core';

import * as THREE from 'three';
import { ThreeService } from '../modules/three/services/three.service';
import * as TWEEN from '@tweenjs/tween.js';


@Injectable()
export class ThreeParserService {

	constructor(private svc:ThreeService) {}

	// star list
	public stars:Array<App.UU.Star> = [];
	
	// current cluster
	public cluster:App.Obj.NameStub;
	
	// focus point of camera
	public focus:App.UU.CamFocus = {star:false,obj:false};	

	// list of textures
	private textures = {};
	
	// orbital parameters
	private orbitalParameters = {

		scale:{

			star:{
				dist:500,
				mass:332946,
				size:1
			},
			objStatic:{
				mass:5.972 * 10^24,
			}

		},
		au:1496e8,
		G:6.67408e-11

	};


	resetScene(){

		this.svc.cleanScene(['skybox','stars']);
	
	}

	getFPS() {

		return this.svc.FPS;

	}

	setAssets(assetArray:{shaders?,images?}) {

		return new Promise((resolve,reject)=>{

			let p = [];
			p.push(new Promise((res,rej)=>{

				if(assetArray.shaders){

					this.svc.addShaders(assetArray.shaders)
					.then(()=>{
						
						res();
					
					})
					.catch(e=>{

						rej(e);

					});

				} else {

					res();

				}

			}));

			p.push(new Promise((res,rej)=>{
				
				if(assetArray.images){

					this.svc.getTextures(assetArray.images)
					.then(textures=>{

						this.textures = textures;
						res();
					
					})
					.catch(e=>{

						rej(e);

					});

				} else {

					res();

				}

			}));

			Promise.all(p)
				.then(()=>{

					resolve();
				
				})
				.catch(e=>{

					reject(e);

				});

		});

	}

	private getTexture(name){

		return this.textures[name];

	}

	setSkyBox(){

		let that = this;
		return new Promise((resolve,reject)=>{

			let texture = that.getTexture('skybox');
			let shaders = that.svc.getShader('skybox');

			if(!texture || !shaders) {

				reject('skybox assets not loaded');
			
			} else {

				let geometry = new THREE.SphereGeometry(10000, 32, 32),
				container = new THREE.Object3D();

				container['uniforms'] = {

					master:{

						skyboxTexture: {
							type: 't', 
							value: texture
						},

					}

				};

				let material = new THREE.ShaderMaterial({
					uniforms:container['uniforms']['master'],
					vertexShader:shaders.vertex,
					fragmentShader: shaders.fragment,
					side:THREE.BackSide
				});

				let skyBox = new THREE.Mesh(geometry, material);
				container.add(skyBox);

				container.rotateY((Math.PI*90)/180);

				container['update'] = obj => {
					obj.rotateY((Math.PI*0.001)/180);
				};

				that.svc.addToCategory('skybox',container);

				resolve();

			}

		});

	}

	setStars(stars){

		let that = this;
		return new Promise((resolve,reject)=>{

			let category = this.svc.scene.getObjectByName('stars');
			if(!category) {

				this.svc.addCategory('stars')
					.then(()=>{

						this.setStars(stars)
							.then(()=>{

								resolve();

							})
							.catch(e=>{

								reject(e);
							
							});

					})
					.catch(e=>{

						reject(e);

					});

			} else {

				// truncate current star list
				let truncatedStarList = [];
				this.stars.forEach(rStar=>{

					let idx = stars.findIndex(x=>x.stub === rStar.stub);
					if(idx > -1){

						// console.log('. not removing',rStar.name);
						truncatedStarList.push(rStar);

					} else {

						// console.log('. removing',rStar.name);
						category.remove(rStar.RenderObj);

					}

				});
				this.stars = truncatedStarList;

				// new position is first star in received star list
				let focusPosMutation = {
					x:stars[0].posX,
					y:stars[0].posY,
					z:stars[0].posZ
				};

				this.focus.star = stars[0].stub;

				// move all current stars to the right positions
				let promiseArray = [];
				this.stars.forEach(cStar=>{

					promiseArray.push(new Promise((res,rej)=>{

						let current = {
							x:cStar.real.x,
							y:cStar.real.y,
							z:cStar.real.z
						};

						let target = {
							x:cStar.posX - focusPosMutation.x,
							y:cStar.posY - focusPosMutation.y,
							z:cStar.posZ - focusPosMutation.z,								
						}

						this.svc.controls.enabled = false;
						let that = this;
						let twMove = new TWEEN.Tween(current)
							.to({x:target.x,y:target.y,z:target.z},1000)
							.easing(TWEEN.Easing.Quadratic.Out)
							.onUpdate(function(){

								cStar.RenderObj.position.set(
									current.x * that.orbitalParameters.scale.star.dist,
									current.y * that.orbitalParameters.scale.star.dist,
									current.z * that.orbitalParameters.scale.star.dist
								);

							})
							.onComplete(function(){

								cStar.real.x = target.x;
								cStar.real.y = target.y;
								cStar.real.z = target.z;
								that.svc.controls.enabled = true;

								res();
							})
							.start();

					}));

				});

				Promise.all(promiseArray)
					.then(()=>{

						// get shaders, textures, etc.
						let textures = {
							shine:that.getTexture('star-shine'),
							surface:that.getTexture('star-surface')
						};
						let shaders = {
							surface:that.svc.getShader('star-surface')
						};

						if(!textures.shine || !textures.surface || !shaders.surface) {

							reject('missing assets');

						} else {
							
							// add all received stars that don't exist
							stars.forEach(star=>{

								let idxOfStar = that.stars.findIndex(x=>x.stub === star.stub);
								if(idxOfStar < 0){

									star.real = {
										
										x:star.posX - focusPosMutation.x,
										y:star.posY - focusPosMutation.y,
										z:star.posZ - focusPosMutation.z,
										r0:star.r0 * this.orbitalParameters.scale.star.size,

									};

									star.RenderObj = that.buildStar(star,textures,shaders);
									star.RenderObj.position.copy(new THREE.Vector3(
										star.real.x * this.orbitalParameters.scale.star.dist,
										star.real.y * this.orbitalParameters.scale.star.dist,
										star.real.z * this.orbitalParameters.scale.star.dist
									));

									category.add(star.RenderObj);
									that.stars.push(star);

								}

							});

							let newFocus = that.stars.findIndex(x=>x.stub === that.focus.star);
							if(newFocus>-1){

								let focalStarObj = that.stars[newFocus].RenderObj;
								that.stars.forEach(rStar=>{

									rStar.RenderOrderDistance = rStar.RenderObj.position.distanceTo(focalStarObj.position);

								});
								that.stars.sort((a,b)=>{
									return a.RenderOrderDistance - b.RenderOrderDistance;
								});

								// MOVE CAMERA TO STAR POS
								// https://stackoverflow.com/questions/25277919/three-js-tween-camera-lookat

								// backup original rotation
								// var startRotation = new THREE.Euler().copy( this.svc.camera.rotation );

								// // final rotation (with lookAt)
								// this.svc.camera.lookAt( that.stars[0].RenderObj.position );
								// var endRotation = new THREE.Euler().copy( this.svc.camera.rotation );

								// // revert to original rotation
								// this.svc.camera.rotation.copy( startRotation );

								// // Tween
								// new TWEEN.Tween( this.svc.camera ).to( { rotation: endRotation }, 600 ).start();

								resolve();

							} else {
								
								reject('error finding focus for distance calculation');
							
							}
							
							
						}
					
					})
					.catch(e=>{

						console.error('ERROR IN MOVING STAR CHILDREN',e);
					
					});

			}
			
		});

	}

	setObjChildren(obj:App.UU.Obj,children:App.UU.Obj[]){}

	setObjects(objs:App.UU.Obj[],focus:string){

		let that = this;
		return new Promise((resolve,reject)=>{

			let starIdx = that.stars.findIndex(x=>x.stub === focus);
			if(starIdx>-1){

				let GM = that.orbitalParameters.scale.star.mass * that.orbitalParameters.scale.objStatic.mass * that.orbitalParameters.G * that.stars[starIdx].mass;

				// divide by top and children
				let top = [], children = [], p = [], coordinates = new THREE.Vector3(0,0,0);
				objs.forEach(obj=>{
					
					if(!obj.parent) {
					
						top.push(obj);
					
					} else {
					
						children.push(obj);
					
					}

				});

				top.forEach(topObj=>{

					// make top obj
					console.log(topObj);

					// child-loop

				});

				resolve();

			} else {

				reject('focus not found');

			}


		});

	}

	buildPlanetaryObj(parent:{x,y,z}|any){

		console.log('buildPlanetaryObj');

		let parentPosition = new THREE.Vector3(0,0,0);
		if(parent){
			parentPosition.x = parent.x;
			parentPosition.y = parent.y;
			parentPosition.z = parent.z;
		}

			// 
			// Required Parameters
			// 


			// override params
			// https://web.archive.org/web/20021014232553/http://www.amsat.org/amsat/keps/kepmodel.html#incl

			// r radius
			// t tilt
			// Ω RAAN
			// w ARGP - Argument of Perigree
			// t tilt
			// i inclination
			// a semi-major axis
			// e eccentricity

			let Ω = (Math.random() * 360) * Math.PI/180;
			let w = (Math.random() * 360) * Math.PI/180;
			let t = (Math.random() * 10) * Math.PI/180;
			let i = (Math.random() *10) * Math.PI/180;
			let a = 10; 
			let e = (Math.random()* 0.25);
			
			// placeholder 
			let auScale = 10;
			let M = 0;


			let r = 1;


			// Time calculatons

			// GM = 1.9891e30 * G * x solar masses = 1.3275413e20
			// T = 2 * pi * sqrt((1496e8 * a ) ^3/ GM )
			
			// 10 minutes = 1 year = 31557600s
			// RTS = T / 600 = 52566666s in game p/real time second
			// DPR = 360/RTS degrees per real time second 

			// Mstart, dt_created from params

			// EPOCHREV = Number of rotations since start = Mstart + ((Date.now() - dt_created) * RTS)

			// https://stackoverflow.com/questions/14494413/javascript-find-out-how-many-times-a-number-goes-into-another-number-evenly
			// EPOCHREV_NOW = (EPOCHREV - (EPOCHREV * Math.floor(EPOCHREV / 360))) * DPR

			// M = EPOCHREV_NOW + (DPR * 1/FPS) 

			// make sure it's not bigger than 360
			// M = M - (M * Math.floor(M/360));

			// From: https://doc.qt.io/qt-5.10/qtcanvas3d-threejs-planets-planets-js.html
			// https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Position_as_a_function_of_time

	        let E = M + e * Math.sin(M) * (1.0 + e * Math.cos(M));

	        let xv = a * (Math.cos(E) - e);
	        let yv = a * (Math.sqrt(1.0 - e * e) * Math.sin(E));

	        let v = Math.atan2(yv, xv);

	        // Calculate the distance (radius)
	        let dist = Math.sqrt(xv * xv + yv * yv);

	        // From http://www.davidcolarusso.com/astro/
	        // Modified to compensate for the right handed coordinate system of OpenGL
	        let xh = dist * (Math.cos(Ω) * Math.cos(v + w)
	                      - Math.sin(Ω) * Math.sin(v + w) * Math.cos(i));
	        let zh = - dist * (Math.sin(Ω) * Math.cos(v + w)
	                       + Math.cos(Ω) * Math.sin(v + w) * Math.cos(i));
	        let yh = dist * (Math.sin(w + v) * Math.sin(i));

	        //
	        // Create object
	        //

			let objContainer:THREE.Object3D = new THREE.Object3D();

	        objContainer.position.set(parentPosition.x + xh * auScale,
	                            parentPosition.y + yh * auScale,
	                            parentPosition.z + zh * auScale);


	        // Apply the position offset from the center of orbit to the bodies
	        // var centerOfOrbit = objects[planet["centerOfOrbit"]];
	        // object.position.set(centerOfOrbit.position.x + xh * auScale,
	        //                     centerOfOrbit.position.y + yh * auScale,
	        //                     centerOfOrbit.position.z + zh * auScale);

			var geometry = new THREE.SphereGeometry( r, 32, 32 );
			var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
			var sphere = new THREE.Mesh( geometry, material );
			objContainer.add( sphere );

			this.stars[0].RenderObj.add(objContainer);

		// }


	}

	buildStar(starObj,textures,shaders){

		let objContainer:THREE.Object3D = new THREE.Object3D();

		let sphereUniforms = {

			master:{
				time:{ type: "f", value: Math.random() },
				surfaceTexture:{ type: "t", value:textures.surface},
				sunColorVec:{type:"v3",value:new THREE.Color('#'+starObj.star_color)}
			},
			options:{
				time:0.05,
			}

		};

		let geometry = new THREE.SphereGeometry( starObj.real.r0, 32, 32 );
		
		let sunShaderMaterial = new THREE.ShaderMaterial( {
			uniforms:sphereUniforms.master,
			vertexShader: shaders.surface.vertex,
			fragmentShader: shaders.surface.fragment
		});

		let sphere = new THREE.Mesh( geometry, sunShaderMaterial );

		sphere.name = 'surface';
		sphere['uniforms'] = sphereUniforms;
		sphere['update'] = obj =>{
			if (this.svc.camera.position.distanceTo(obj.position.clone()) < 101) obj.rotateY(Math.PI*0.05/180);
		}
		objContainer.add(sphere);
		objContainer.name = starObj.stub;

		objContainer.rotateY((Math.PI*90)/180);

		return objContainer;

	}

}

