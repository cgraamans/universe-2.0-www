import { TestBed, inject } from '@angular/core/testing';

import { ThreeParserService } from './three-parser.service';

describe('ThreeParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThreeParserService]
    });
  });

  it('should be created', inject([ThreeParserService], (service: ThreeParserService) => {
    expect(service).toBeTruthy();
  }));
});
