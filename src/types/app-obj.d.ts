declare namespace App.Obj {

	export interface NameStub {

		name:string;
		stub:string;

	}

	export interface NameUrl {
		name:string;
		url:string;
	}

}