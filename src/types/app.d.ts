declare namespace App {

	export interface FormObj {

		name?:number;
		password?:number;
		persistent:boolean;

	}

	export interface FormObjValid {

		name:boolean;
		password:boolean;

	}

	export interface Coordinates {
	
		x:number;
		y:number;
		z:number;
	
	}

}