declare namespace App.UU {

	export interface Star {

		stub:string;
		name:string;
		RenderObj?:THREE.Object3D;
		real:App.Coordinates;
		posX:number;
		posY:number;
		posZ:number;
		RenderOrderDistance:number;

		mass:number;

	}

	export interface CamFocus {
	
		star:string|false;
		obj:string|false;

	}

	export interface Obj {

		stub:string;
		name:string;
		'type':string;

		size:string|null;

		r:number|null;
		a:number|null;
		e:number|null;
		i:number|null;
		w:number|null;
		'Ω':number|null;
		M:number|null;
		t:number|null;

		mass:number|null;
		isSolid:number;
		percentWater:number;

		coordinates:App.Coordinates|null;

		parent:string|null;

	}

}

declare namespace App.UU.TS {

	export interface FileStore {

		shaders:Shaders;
		textures:TextureDetailLevels;

	}

	export interface TextureDetailLevels {

		low:Textures;
		high?:Textures;

	}

	export interface Shader {

		name:string;
		url:string;
		type:string;

	}

	export interface Shaders {

		stars:Array<Shader>;
		skybox:Array<Shader>;

	}

	export interface Texture {

		name:string;
		url:string;
		texture?:THREE.Texture;

	}

	export interface Textures {

		stars:Array<Texture>;
		tracks:Array<Texture>;

	}

}