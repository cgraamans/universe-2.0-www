declare namespace App.ViewPort {

	export interface FocusObj {

		cluster?:{name,stub};
		star?:{name,stub};

	}

	export interface RouteParams {

		cluster?:string;
		star?:string;
		obj?:string; 

	}

}